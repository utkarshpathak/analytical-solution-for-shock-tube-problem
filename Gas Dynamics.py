# Program to compute and plot the analytical  
# solution to the Sod shock-tube problem

import numpy as n #library used
import pylab as pl #library used
import math #library used

p1=input('Enter pressure value on left hand side of the diaphragm(in atm): ') 
p1=p1*101325 # conversion to Pa

p2=input('Enter pressure value on right hand side of the diaphragm(in atm): ')
p2=p2*101325 # conversion to Pa

t1=input('Enter the temperature value on left hand side of the diaphragm: ')
t2=input('Enter the temperature value on right hand side of the diaphragm: ')

g=input('Enter the value of gamma: ')
r=input('Enter the value of R (gas constant): ')
cp=input('Enter the value of cp(specific heat): ')

a1=(g*r*t1)**0.5 #velocity of sound in the left section
a2=(g*r*t2)**0.5 #velocity of sound in the right section

udiff=100.0 #parameter used for running the below loop

p3=(p1)/1.05 #pressure value to start the iteration with

while udiff>0.01 : #condition to keep the loop running   
     p3=p3-100 #reduction in pressure by 100 Pa for every loop
     u3=2*a1*(1/(g-1))*(1-((p3/p1)**((g-1)/(2*g)))) #velocity after accelaration through expansion fan
     m2=(((p3/p2)*(g+1)+(g-1))/(2*g))**0.5 #mach no. of shock wave
     m4=((((g-1)*m2*m2)+2)/(2*g*m2*m2-(g-1)))**0.5 #mach no. of gas after going through shock wave (stationary frame)
     t4=t2*((1+((g-1)*0.5*m2*m2))/(1+(g-1)*0.5*m4*m4)) #temperature achieved after the shock wave
     a4=(g*r*t4)**0.5 #velocity of sound after the shock wave 
     u4=(m2*a2)-(m4*a4) #induced velocity after the passing of shock wave
     udiff=u4-u3 #the induced velocity and velocity after accelaration to through expansion fan must be equal  
pass;

t3=t1*((p3/p1)**((g-1)/(g))) #temperature of gas after accelaration through the expansion fan
u4=u3 #the velocity across the contact wave is same
p4=p3 #the pressure across the contact wave is same
u2=m2*a2 #velocity of shock wave
u1=a1 #velocity of expansion fan head
utail=((g*r*t3)**0.5)-u3 #velocity of expansion fan tail
time=input('Enter the value of time in milli-seconds ') 
time=time/1000.0 #convert to seconds
x=n.linspace(0,1.000,1001) #declare the array that represents the shock tube
p=n.zeros(1001) #array for pressure
t=n.zeros(1001) #array for temperature
rho=n.zeros(1001) #array for density
v=n.zeros(1001) #array for velocity
m=n.zeros(1001) #array for mach no.
xshock=int(math.floor(1000*((u2*time)+0.5))) #place where shock wave is present in the shock tube
xcontact=int(math.floor(1000*((u4*time)+0.5))) #place where contact wave is present in the shock tube
xhead=int(math.floor(1000*((-1*u1*time)+0.5))) #place where EF head is present in the shock tube
xtail=int(math.floor(1000*((-1*utail*time)+0.5))) #place where EF tail is present in the shock tube

for i in range(0,1001): #loop for filling the values at every point of the shock tube 
    if(i<=xhead): #standard equations used
        p[i]=p1
        t[i]=t1
        rho[i]=p1/(r*t1) 
        v[i]=0.0
        m[i]=(v[i])/((g*r*t1)**(0.5))
    if(i>xhead and i<=xtail): #standard equations used
        v[i]=(u3)*(x[i]-x[xhead])/(x[xtail]-x[xhead]) #linear variation of velocity assumed
        t[i]=t1*(((1-(((g-1)/2)*(v[i]/a1))))**2)
        p[i]=p1*(t[i]/t1)**(g/(g-1))
        rho[i]=rho[1]*(t[i]/t1)**(1/(g-1))
        m[i]=(v[i])/((g*r*t[i])**(0.5))
    if(i>xtail and i<=xcontact): #standard equations used
        v[i]=u3
        t[i]=t3
        p[i]=p3
        rho[i]=p[i]/(r*t[i])
        m[i]=(v[i])/((g*r*t[i])**(0.5))
    if(i>xcontact and i<=xshock): #standard equations used
        v[i]=u4
        t[i]=t4
        p[i]=p4
        rho[i]=p[i]/(r*t[i])
        m[i]=(v[i])/((g*r*t[i])**(0.5))
    if(i>xshock): #standard equations used
        v[i]=0
        t[i]=t2
        p[i]=p2
        rho[i]=p[i]/(r*t[i])
        m[i]=(v[i])/((g*r*t[i])**(0.5))
pass;

pmax=max(p) #finding max. value for normalisation
vmax=max(v) #finding max. value for normalisation
tmax=max(t) #finding max. value for normalisation
rhomax=max(rho) #finding max. value for normalisation
mmax=max(m) #finding max. value for normalisation

for j in range(0,1001):
    p[j]=p[j]/pmax #normalising the variable array
    v[j]=v[j]/vmax #normalising the variable array  
    t[j]=t[j]/tmax #normalising the variable array   
    rho[j]=rho[j]/rhomax #normalising the variable array    
    m[j]=m[j]/mmax #normalising the variable array
pass;

pl.title("Shock tube problem") #plotting the graphs
pl.plot(x,t,"-r",label='Temperature')
pl.plot(x,p,"-g",label='Pressure')
pl.plot(x,rho,"-b",label='Density')
pl.plot(x,v,"-m",label='Velocity')
pl.plot(x,m,"-k",label='Mach no.')
pl.xlabel("Tube length (x) -->")
pl.ylabel("Normalized variables -->")
pl.grid()
pl.legend(loc='center left')            
