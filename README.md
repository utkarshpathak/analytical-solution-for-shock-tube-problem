# Analytical solution for Shock tube problem

A python program to compute and plot flow variables (density, velocity, pressure, Mach no. and temperature) for the Sod shock
tube problem.